from django import forms
from django.forms import Textarea, TextInput

class feedbackform(forms.Form):
    error_messages = {
        'required': 'This field is required.'
    }
    attrs = {
        'class' : 'form-control'
    }
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label='Nama ', max_length=50, required=True)
    email = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label = 'Email ', max_length=30, required=True)
    message = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control'}), label = 'Message ',max_length=500, required=True)